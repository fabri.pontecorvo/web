package com.fpontecorvo.web;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.fpontecorvo.web.entity.Usuario;
import com.fpontecorvo.web.repository.UsuarioRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WebApplicationTests {
	
	@Autowired
	private UsuarioRepository repo;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Test
	public void crearUsuario() {
		Usuario user = new Usuario();
		
		user.setId(Long.getLong("1"));
		user.setUsuario("fabri");
		user.setPassword(encoder.encode("1234"));
		Usuario retorno = repo.save(user);
		
		assertTrue(retorno.getPassword().equals(user.getPassword()));
	}
	
	

}
