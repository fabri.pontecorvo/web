package com.fpontecorvo.web.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fpontecorvo.web.entity.Usuario;
import com.fpontecorvo.web.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements UsuarioService,UserDetailsService {
    private static Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);
    
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
		Usuario usuario = usuarioRepository.findByUsuario(user);
		if (usuario == null) {
			throw new UsernameNotFoundException(user);
		}
		
        log.info(usuario.toString());
    	List<GrantedAuthority> roles = new ArrayList<>();
		roles.add( new SimpleGrantedAuthority("ADMIN"));
		
		UserDetails userDet =  new User(usuario.getUsuario(),usuario.getPassword(),roles);
		
		return userDet;
	}

}
