package com.fpontecorvo.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class WebApplication {
    private static Logger log = LoggerFactory.getLogger(WebApplication.class);

	public static void main(String[] args) {
		log.info("INICIANDO....");
		SpringApplication.run(WebApplication.class, args);
		log.info("LEVANTADO....");
	}

}
