package com.fpontecorvo.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fpontecorvo.web.entity.Cliente;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	

}
