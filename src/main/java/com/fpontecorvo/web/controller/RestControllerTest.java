package com.fpontecorvo.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fpontecorvo.web.entity.Cliente;
import com.fpontecorvo.web.repository.ClienteRepository;

@RestController
@RequestMapping("/rest")
public class RestControllerTest {

	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping
	public List<Cliente> ListarClientes() {
		return this.clienteRepository.findAll();
	}
	
	@PostMapping
	public void insertarCliente(@RequestBody Cliente cliente) {
		clienteRepository.save(cliente);
	}
	
	@PutMapping
	public void updateCliente(@RequestBody Cliente cliente) {
		clienteRepository.save(cliente);
	}
	
	@DeleteMapping(value= "/{id}")
	public void deleteCliente(@PathVariable("id") Long id) {
		clienteRepository.deleteById(id);
	}
	
}
