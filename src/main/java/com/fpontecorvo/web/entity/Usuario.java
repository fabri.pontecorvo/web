package com.fpontecorvo.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
@Entity
@Table(name = "usuario", schema = "seguridad")
public class Usuario extends AbstractEntity {

	@Id
	@GeneratedValue(generator = "seguridad.usuario_id_seq")
	@SequenceGenerator(name = "seguridad.usuario_id_seq", sequenceName = "seguridad.usuario_id_seq", initialValue = 1)
	private Long id;

	@Column(name = "usuario", nullable = false, unique = true)
	private String usuario;

	@Column(name = "password")
	private String password;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", usuario=" + usuario + ", password=" + password + "]";
	}
	
	
	
	
}
