package com.fpontecorvo.web.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "cliente", schema="public")
public class Cliente extends AbstractEntity {

	@Id
	@GeneratedValue(generator = "cliente_id_seq", strategy=GenerationType.AUTO)
	@SequenceGenerator(name = "cliente_id_seq", sequenceName = "cliente_id_seq")
	private Long id;

	@Column(name = "nombre")
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
